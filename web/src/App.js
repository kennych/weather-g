import React, { Component } from 'react';
import './App.css';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import {Hiking} from './components/Hiking'
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import FlatButton from 'material-ui/FlatButton';
class App extends Component {
  constructor(){
   super()
   this.state = {
       weather:{
         'data':{'main':{'temp':''}}
       },
       weather2:{
         'dt_txt':'',
         'main':{'temp':''}
       },
       weather3:{
         'dt_txt':'',
         'main':{'temp':''}
       },
       title: "Hiking Weather",
       current_hike: "Lindeman Lake",
       hikes:['Lindeman Lake','Joffres Lake','St Marks Summit','East Canyon Trail','Crystal Falls','Tynehead'],
       hike_index: 0,
   }

   this.handleClick = this.handleClick.bind(this);
   this.getData = this.getData.bind(this);

 }

 handleClick (evt) {
   if(this.state.hike_index!=5){
     this.state.hike_index = this.state.hike_index + 1
     this.state.current_hike = this.state.hikes[this.state.hike_index]
   }
   else{
     this.state.hike_index=0
     this.state.current_hike = this.state.hikes[this.state.hike_index]
   }
     this.getData()
   }

getData(){
  let dat = ""
  dat = fetch('/getWeather?'+'hike_name='+this.state.current_hike) .then((response) => response.json()) .then((responseJson) => {
      this.setState({weather:responseJson})
      return responseJson;
    })
    dat = fetch('/getForecast?'+'hike_name='+this.state.current_hike) .then((response) => response.json()) .then((responseJson) => {
      responseJson = responseJson.data.list
        this.setState({weather2:responseJson[8],
          weather3:responseJson[15]

        })

        return responseJson;
      })
}

 componentDidMount(){
   this.getData()

}

  render() {
    return (
      <MuiThemeProvider >
      <div className="App">
        <div className="App-header">
          <h2 className="App-title">{this.state.title}</h2>
        </div>
        <div className="outer-div">
        <p className="App-intro">
          {this.state.current_hike}
        </p>

        <FlatButton onClick={this.handleClick} className="buttonFlat">Next</FlatButton>
        </div>
        <Hiking weather={this.state.weather} weather2={this.state.weather2} weather3={this.state.weather3}></Hiking>
      </div>
      </MuiThemeProvider>
    );
  }
}

export default App;
