import React from 'react'
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';


export const Hiking = (props) => {
  return (
    <div className="hiking-div">

      <div className="row-one">

        <div className='r1-c1'>
          <p>Current Weather</p>
          <p>Temperature: {props.weather.data.main.temp}</p>

        </div>

      </div>


      <div className="row-two">
        <div className='r2-c1'>
              <p>Upcoming Weather</p>
              <p>Time:{props.weather2.dt_txt}</p>
              <p>Temperature: {props.weather2.main.temp} </p>

        </div>
        <div className='r2-c2'>
              <p>Upcoming Weather</p>
              <p>Time:{props.weather3.dt_txt}</p>
              <p>Temperature: {props.weather3.main.temp} </p>
        </div>
        
        <br></br>
        <p>Todo: 1) Add Air Quality, 2) Add More Data Items</p>

      </div>


    </div>
  )
}
