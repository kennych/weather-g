from bottle import route,hook, run,request,response,abort,get,post,static_file,template,error

from weather import weather
from hikes import hikes
import json


@route('/static/js/<filename>')
def server_js(filename):
    return static_file(filename, root='./web/build/static/js')

@route('/static/css/<filename>')
def server_css(filename):
    return static_file(filename, root='./web/build/static/css')

@route('/static/media/<filename>')
def server_media(filename):
    return static_file(filename, root='./web/build/static/media')

@route('/')
def index():
    return template('web/build/index.html')

weather = weather.Weather()
all_hikes = hikes.Hikes().get_hikes()

@route('/getWeather')
def get_weather():
    hike_name = request.query.hike_name
    data_json = None
    try:
        weather.set_id(all_hikes[hike_name]['id'])
        data_json = weather.get_today_forecast()
    except KeyError:
        data_json = {"error":"hike not in memory"}
    return json.dumps(data_json)

@route('/getForecast')
def get_weather():
    hike_name = request.query.hike_name
    data_json = None
    try:
        weather.set_id(all_hikes[hike_name]['id'])
        data_json = weather.get_future_forecast()
    except KeyError:
        data_json = {"error":"hike not in memory"}
    return json.dumps(data_json)

run(host='0.0.0.0', port=81, debug=True,server='cherrypy')