from datetime import date
from datetime import datetime
import requests

class Weather:
    'BaseClass For Weather API'

    #todo: if time permits load with environment variables
    __api_key = '284dca80ada8869218c7df6494e52435'

    def __init__(self):
        self._id=None
        self._current_date=date.isoformat(datetime.now())
        self._r=requests

    def get_today_forecast(self):
        datum = {'status':'n/a','data':{}}

        if(self.get_id() is not None):
            url = 'http://api.openweathermap.org/data/2.5/{0}?id={1}&APPID={2}&units=metric'.format('weather',self.get_id(),self.get_api())
            data = self._r.get(url)
            datum['status']=self.handle_status(data.status_code)
            datum['data'] = data.json()

        return datum

    def get_future_forecast(self):
        datum = {'status': 'n/a', 'data': {}}
        if (self.get_id() is not None):
            url = 'http://api.openweathermap.org/data/2.5/{0}?id={1}&APPID={2}&units=metric'.format('forecast',self.get_id(),self.get_api())
            data = self._r.get(url)
            datum['status']=self.handle_status(data.status_code)
            datum['data'] = data.json()
        return datum

    def set_id(self,id):
        self._id = id

    def get_id(self):
        return self._id

    def get_api(self):
        return self.__api_key

    def handle_status(self,status_code):
        code = str(status_code)
        status_msg = None
        if(code=='200'):
            status_msg = "API Call Successful"
        else:
            status_msg = "API Call Failure"
        return status_msg