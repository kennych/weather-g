from datetime import date
import requests

class Hikes:
    'BaseClass For Hikes API'

    #todo: if time permits load with environment variables

    def __init__(self):
        self._hikes={
            'Lindeman Lake':{
                'id':'5921356'
            },
            'Joffres Lake':{
                'id':'6100799'
            },
            'St Marks Summit': {
                'id': '6090785'
            },
            'East Canyon Trail': {
                'id': '6065686'
            },
            'Crystal Falls': {
                'id': '5927689'
            },
            'Tynehead': {
                'id': '6159905'
            }
        }

    def get_hike(self,hike):
        return self._hikes[hike]

    def get_hikes(self):
        return self._hikes