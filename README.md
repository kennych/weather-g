# README #

If I could put more time into this, I'd add more error handling on the client side, unit tests for the client side, and unit tests for the rest api. Plan is to include a few as an example.

### What is this repository for? ###

* Weather APP
* Version 1.0


### How do I get set up? ###

1. pip install bottle
2. pip install "cherrypy>=3.0.8,<9.0.0"
3. pip install requests
4. git clone git@bitbucket.org:kennych/weather-g.git
5. cd weather-g
6. python main.py
7. visit http://localhost:81 (put on port 81 to avoid interfering with 80).

demo server: http://199.167.20.251:81/